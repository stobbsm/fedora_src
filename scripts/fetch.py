#!/usr/bin/env python3

## Download list of projects to then pipe into git + parallel
import json
import requests
import multiprocessing
from multiprocessing.dummy import Pool as ThreadPool
import subprocess
import os

current='https://src.fedoraproject.org/api/0/projects?namespace=rpms&per_page=100&page={}'
repobase='https://src.fedoraproject.org/{}.git'
storebase='submodules/{}'
projects_filename = 'projects.json'
projects = {}

cpus = multiprocessing.cpu_count()

r = requests.get(current.format(1))
pages = int(json.loads(r.content)["pagination"]["pages"])
urls = []
for p in range(1, pages):
    urls.append(current.format(p))

print("Generated URLS")
pool = ThreadPool(cpus)
results = pool.map(requests.get, urls)
pool.close()
pool.join()
for r in results:
    _r = json.loads(r.content)
    for p in _r["projects"]:
        projects[p["name"]] = p["url_path"]

cmds = []
for p in projects:
    cmd = [
        'git',
        'submodule',
        'add',
        repobase.format(projects[p]).rstrip('/'),
        storebase.format(projects[p])
    ]
    cmds.append(cmd)

for cmd in cmds:
    result = subprocess.call(cmd)